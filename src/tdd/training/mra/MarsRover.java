package tdd.training.mra;

import java.util.ArrayList;
import java.util.List;

public class MarsRover {
	
	private StringBuilder roverStatus;
	private List<String> planetObstacles = new ArrayList<>();
	private final static StringBuilder emptyString = new StringBuilder("");
	
	/**
	 * It initializes the rover at the coordinates (0,0), facing North, on a planet
	 * (represented as a grid with x and y coordinates) containing obstacles.
	 * 
	 * @param planetX         The x dimension of the planet.
	 * @param planetY         The y dimension of the planet.
	 * @param planetObstacles The obstacles on the planet. Each obstacle is a string
	 *                        (without white spaces) formatted as follows:
	 *                        "(oi_x,oi_y)". <code>null</code> if the planet does
	 *                        not contain obstacles.
	 * 
	 * @throws MarsRoverException
	 */
	public MarsRover(int planetX, int planetY, List<String> planetObstacles) throws MarsRoverException {
		this.planetObstacles = createPlanet(planetX, planetY,planetObstacles );
		roverStatus = new StringBuilder("(0,0,N)");
	}

	/**
	 * It returns whether, or not, the planet (where the rover moves) contains an
	 * obstacle in a cell.
	 * 
	 * @param x The x coordinate of the cell
	 * @param y The y coordinate of the cell
	 * @return <true> if the cell contains an obstacle, <false> otherwise.
	 * @throws MarsRoverException
	 */
	public boolean planetContainsObstacleAt(int x, int y) throws MarsRoverException {
		
		for (int i=0; i<planetObstacles.size(); i++) {
			if(planetObstacles.get(i).compareTo("oi_"+x+",oi_"+y) == 0 ) {
				return true;
			}
		}
		return false;
	}

	/**
	 * It lets the rover move on the planet according to a command string. The
	 * return string contains the new position of the rover, its direction, and the
	 * obstacles it has encountered while moving on the planet (if any).
	 * 
	 * @param commandString A string that can contain a single command -- i.e. "f"
	 *                      (forward), "b" (backward), "l" (left), or "r" (right) --
	 *                      or a combination of single commands.
	 * @return The return string that contains the position and direction of the
	 *         rover, and the obstacles the rover has encountered while moving on
	 *         the planet (if any). The return string (without white spaces) has the
	 *         following format: "(x,y,dir)(o1_x,o1_y)(o2_x,o2_y)...(on_x,on_y)". x
	 *         and y define the new position of the rover while dir represents its
	 *         direction (i.e., N, S, W, or E). Finally, oi_x and oi_y are the
	 *         coordinates of the i-th encountered obstacle.
	 * @throws MarsRoverException
	 */
	public String executeCommand(String commandString) throws MarsRoverException {
		
		StringBuilder command = new StringBuilder(commandString);
		for(int i=0; i<commandString.length(); i++) {
			if(command.compareTo(emptyString)==0) {
				return roverStatus.toString();
			}else if(command.charAt(i) == 'r') {
				roverStatus.setCharAt(roverStatus.length()-2, 'E');
				System.out.println(roverStatus);
				 
			}else if(command.charAt(i) == 'l') {
				roverStatus.setCharAt(roverStatus.length()-2, 'W');
				System.out.println(roverStatus);
				
			}else if(command.charAt(i) == 'f') {
				moveRoverForward();
				System.out.println(roverStatus);
				  
			}else if(command.charAt(i) == 'b') {
				moveRoverBackward();
				System.out.println(roverStatus);
				  
			}
		
		}
		return roverStatus.toString(); 
	}
	
	public List<String> createPlanet(int planetX, int planetY,List<String> planetObstacles ){
		List <String> planet = new ArrayList<>();
		int z=0;
		
		for(int i=0; i<planetY; i++) {
			for(int j=0; j<planetX; j++) {
				
				
				if(z<planetObstacles.size() && planetObstacles.get(z).compareTo(j+","+i) == 0  ){
					
					planet.add("oi_" + j + ",oi_" + i);
					z++;
				}else {
					planet.add(j+","+i);
				}
			}
		}
		
		return planet;
	}
	
	public StringBuilder getRoverStatus() {
		return roverStatus;
	}

	public void setRoverStatus(StringBuilder roverStatus) {
		this.roverStatus = roverStatus;
	}
	
	private void moveRoverForward() {
		List<Integer> coordinates = receiveCoordinates();
		
		if(roverStatus.charAt(roverStatus.length()-2) == 'N') {
			if(coordinates.get(0) < 10 && coordinates.get(1) < 10 
					&& coordinates.get(0) >= 0 && coordinates.get(1) >= 0) {
				coordinates.set(1,coordinates.get(1)+1);
				
			}
		}else if(roverStatus.charAt(roverStatus.length()-2) == 'S') {
			if(coordinates.get(0) < 10 && coordinates.get(1) < 10 
					&& coordinates.get(0) >= 0 && coordinates.get(1) >= 0) {
				coordinates.set(1,coordinates.get(1)-1);
			}
		}else if(roverStatus.charAt(roverStatus.length()-2) == 'E') {
			if(coordinates.get(0) < 10 && coordinates.get(1) < 10 
					&& coordinates.get(0) >= 0 && coordinates.get(1) >= 0) {
				coordinates.set(0,coordinates.get(0)+1);
			}
		}else if(roverStatus.charAt(roverStatus.length()-2) == 'W') {
			if(coordinates.get(0) < 10 && coordinates.get(1) < 10 
					&& coordinates.get(0) >= 0 && coordinates.get(1) >= 0) {
				coordinates.set(0,coordinates.get(0)-1);
			}
		}
		
		roverStatus = new StringBuilder("("+coordinates.get(0)+","+coordinates.get(1)+","+getFacing()+")");
		
	}
	
	private void moveRoverBackward() {
		List<Integer> coordinates = receiveCoordinates();
		
		if(roverStatus.charAt(roverStatus.length()-2) == 'N') {
			if(coordinates.get(0) < 10 && coordinates.get(1) < 10 
					&& coordinates.get(0) >= 0 && coordinates.get(1) >= 0) {
				coordinates.set(1,coordinates.get(1)-1);
				
			}
		}else if(roverStatus.charAt(roverStatus.length()-2) == 'S') {
			if(coordinates.get(0) < 10 && coordinates.get(1) < 10 
					&& coordinates.get(0) >= 0 && coordinates.get(1) >= 0) {
				coordinates.set(1,coordinates.get(1)+1);
			}
		}else if(roverStatus.charAt(roverStatus.length()-2) == 'E') {
			if(coordinates.get(0) < 10 && coordinates.get(1) < 10 
					&& coordinates.get(0) >= 0 && coordinates.get(1) >= 0) {
				coordinates.set(0,coordinates.get(0)-1);
			}
		}else if(roverStatus.charAt(roverStatus.length()-2) == 'W') {
			if(coordinates.get(0) < 10 && coordinates.get(1) < 10 
					&& coordinates.get(0) >= 0 && coordinates.get(1) >= 0) {
				coordinates.set(0,coordinates.get(0)+1);
			}
		}
		
		roverStatus = new StringBuilder("("+coordinates.get(0)+","+coordinates.get(1)+","+getFacing()+")");
		
	}
	private List<Integer> receiveCoordinates() {
		List<Integer> coordinates = new ArrayList<>();
		StringBuilder numero = new StringBuilder("");
		for(int i=0; i<roverStatus.length(); i++) {
			
			if(i<roverStatus.length() && isNumber(i)) {
				numero.append(roverStatus.charAt(i));
				if(roverStatus.charAt(i+1) == ',') {
					coordinates.add(Integer.parseInt(numero.toString()));
					numero.delete(0, numero.length());
				}
			}
			
			
		}
		
		return coordinates;
		
	}
	
	private boolean isNumber(int i) {
		return  roverStatus.charAt(i) == '0' || roverStatus.charAt(i) == '1' || roverStatus.charAt(i) == '2' ||
				roverStatus.charAt(i) == '3' || roverStatus.charAt(i) == '4' || roverStatus.charAt(i) == '5' ||
				roverStatus.charAt(i) == '6' || roverStatus.charAt(i) == '7' || roverStatus.charAt(i) == '8' ||
				roverStatus.charAt(i) == '9';
	}
	
	private Character getFacing() {
		return roverStatus.charAt(roverStatus.length()-2);
		
	}

}
