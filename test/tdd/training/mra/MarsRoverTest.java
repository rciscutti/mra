package tdd.training.mra;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class MarsRoverTest {

	@Test
	public void testCreatingRoverWithObstacleAtXAndY() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("2,3");
		planetObstacles.add("4,7");
		
		MarsRover rover = new MarsRover(10,10, planetObstacles);
		
		
		assertTrue(rover.planetContainsObstacleAt(4, 7));
	}
	
	@Test
	public void testCreatingRoverWithoutObstacleAtXAndY() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("2,3");
		planetObstacles.add("4,7");
		
		MarsRover rover = new MarsRover(10,10, planetObstacles);
		
		
		assertFalse(rover.planetContainsObstacleAt(5, 7));
	}
	@Test
	public void testLandingStatusWithCommandEmpty() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("2,3");
		planetObstacles.add("4,7");
		
		MarsRover rover = new MarsRover(10,10, planetObstacles);
		
		
		assertEquals("(0,0,N)", rover.executeCommand(""));
	}
	
	@Test
	public void testLandingStatusWithTurningRight() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("2,3");
		planetObstacles.add("4,7");
		
		MarsRover rover = new MarsRover(10,10, planetObstacles);
		
		
		assertEquals("(0,0,E)", rover.executeCommand("r"));
	}
	
	@Test
	public void testLandingStatusWithTurningLeft() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("2,3");
		planetObstacles.add("4,7");
		
		MarsRover rover = new MarsRover(10,10, planetObstacles);
		
		
		assertEquals("(0,0,W)", rover.executeCommand("l"));
	}
	
	@Test
	public void testLandingStatusWithForward() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("2,3");
		planetObstacles.add("4,7");
		
		MarsRover rover = new MarsRover(10,10, planetObstacles);
		rover.setRoverStatus(new StringBuilder("(7,6,N)"));
		
		assertEquals("(7,7,N)", rover.executeCommand("f"));
	}
	
	@Test
	public void testLandingStatusWithBackward() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("2,3");
		planetObstacles.add("4,7");
		
		MarsRover rover = new MarsRover(10,10, planetObstacles);
		rover.setRoverStatus(new StringBuilder("(5,8,E)"));
		
		assertEquals("(4,8,E)", rover.executeCommand("b"));
	}
	
	@Test
	public void testLandingStatusWithCombinationCommands() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("2,3");
		planetObstacles.add("4,7");
		
		MarsRover rover = new MarsRover(10,10, planetObstacles);
		rover.setRoverStatus(new StringBuilder("(0,0,N)"));
		
		assertEquals("(2,2,E)", rover.executeCommand("ffrff"));
	}
}
